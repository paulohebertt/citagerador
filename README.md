[favicon]: https://gitlab.com/paulohebertt/citagerador/-/raw/master/public/landscape.svg?inline=false "CitaGerador - Logo"
[logo]: https://gitlab.com/paulohebertt/citagerador/-/raw/master/public/logo.svg?inline=false "CitaGerador"

![CitaGerador - Logo][favicon]

---

![CitaGerador][logo]

---

*CitaGerador - Wikiquote*
[![pipeline status](https://gitlab.com/paulohebertt/citagerador/badges/master/pipeline.svg)](https://gitlab.com/paulohebertt/citagerador/-/commits/master)
[![coverage report](https://gitlab.com/paulohebertt/citagerador/badges/master/coverage.svg)](https://gitlab.com/paulohebertt/citagerador/-/commits/master)


# :sparkles: CitaGerador - Wikiquote

`Perfil:`:sparkles:@paulohebertt

`Projeto:` paulohebertt/citagerador>

[`🧐Demonstração`](https://gitlab.com/paulohebertt/citagerador)

<hr/>
<!---
- [x] Completed task
- [ ] Incomplete task
  - [ ] Sub-task 1
  - [x] Sub-task 2
  - [ ] Sub-task 3

1. [x] Completed task
1. [ ] Incomplete task
   1. [ ] Sub-task 1
   1. [x] Sub-task 2
```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```


```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```
-->
