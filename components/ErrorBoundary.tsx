import React, { Component, ErrorInfo, ReactNode } from "react"

interface Props {
  children: ReactNode
}

interface State {
  hasError: boolean
  errorMessage: string
  errorInfo?: string
}

class ErrorBoundary extends Component<Props, State> {
  state = {
    hasError: false,
    errorMessage: '',
    errorInfo: ''
  }
  static getDerivedStateFromError(e: Error): State {
    return { hasError: true, errorMessage: e.message, errorInfo: e.stack }
  }
  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.error("Error:", error, errorInfo)
  }
  render() {
    const { hasError, errorInfo,errorMessage } = this.state
    if (hasError) {
      return <h1>Ops..., Ocorreu um Erro com o Componente {errorInfo}: {errorMessage}</h1>
    }
    return this.props.children
  }
}

export default ErrorBoundary

