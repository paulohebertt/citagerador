import { Stack, Text,Link, IStackTokens, IStackStyles, Icon } from '@fluentui/react'
import * as icones from 'utils/icones.json'

const stackStyles: IStackStyles = {
    root: {
        background: 'radial-gradient( circle farthest-corner at 45% 55%,  #007caa 0%, #2a5083 100% )',
        color: 'rgb(228 247 255)'
    }
}

const tokenStack: IStackTokens = {
    childrenGap: 10,
    padding: 10,
}

const Footer = () => (
    <Stack horizontal styles={stackStyles} as='footer' tokens={tokenStack}>
        <Text variant={'large'}>Construido com</Text>
        <Stack.Item>
            <img width="17" alt="Nextjs-logo" src={icones.Next} />
            <Link variant={'large'}>Next.js</Link>
        </Stack.Item>
        <Icon iconName="CalculatorAddition" />
        <Stack.Item>
            <Icon iconName="WindowsLogo" />
            <Text variant={'large'}>Fluent UI</Text>
        </Stack.Item>
    </Stack>
)

export default Footer
