import Link from 'next/link'
import { Stack, DefaultEffects ,DefaultPalette, FontSizes, IStackStyles } from '@fluentui/react'

const navStyles: IStackStyles = {
    root: {
        background: 'radial-gradient( circle farthest-corner at 45% 55%,  #007caa 0%, #2a5083 100% )',
        height: 50,
        boxShadow: DefaultEffects.elevation64,
        color: DefaultPalette.white,
    },
}

const styleItem: IStackStyles = {
    root: {
        margin:'0 auto',
        fontSize: FontSizes.xLargePlus
    }
}

const Header = () => (
    <header>
        <Stack horizontal disableShrink as='nav' styles={navStyles} verticalAlign='center' horizontalAlign='center'>
            <Stack.Item styles={styleItem}>
                <h1>GenCitacao👋</h1>
            </Stack.Item>
            <Stack.Item>
                <Link href="/">
                    <a>Início</a>
                </Link>
                <Link href="/about">
                    <a>Aleatório</a>
                </Link>
                <Link href="/about">
                    <a>top 10</a>
                </Link>
                <Link href="/about">
                    <a>Sobre</a>
                </Link>
            </Stack.Item>
        </Stack>
    </header>
)

export default Header
