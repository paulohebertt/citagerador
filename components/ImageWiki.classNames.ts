import { mergeStyles } from '@fluentui/react'

export const getImageWikiClassNames = () => mergeStyles({
    borderRadius: 10,
    objectFit: 'cover',
    displayName: 'imgWiki'
})
