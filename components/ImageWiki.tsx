import { useEffect, useState } from 'react'
import { getImage } from 'utils/fetchers'
import { getImageWikiClassNames } from './ImageWiki.classNames'

const imgState = {
    source: '/citagerador/favicon.png',
    width: 50,
    height: 50
}

type ImageWTypes = {
    id: number,
    title: string
}

const ImageWiki = ({ id, title }: ImageWTypes) => {
    const [img, setImg] = useState(imgState)
    const css = getImageWikiClassNames()
    useEffect(() => {
        const imgWiki = async () => {
            const _img = await getImage(id, title.charAt(0))
            setImg(_img || imgState)
        }
        imgWiki()
    }, [id, title])
    return (
        <img
            src={img.source}
            className={css}
            alt={title}
            width={img.width}
            height={img.height}
        />
    )
}

export default ImageWiki
