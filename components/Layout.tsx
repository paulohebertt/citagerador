import { FC } from 'react'
import { Stack, IStackStyles } from '@fluentui/react'
import Header from './Header'
import Footer from './Footer'

const containerStyles: IStackStyles = {
    root: {
        minHeight: '100vh',
        height: '-webkit-fill-available',
        overflow: 'hidden',
        '& > main > div': {
            width: '100%'
        }
    }
}

const Layout: FC = ({ children }) => (
    <Stack styles={containerStyles} verticalFill>
        <Header />
        <Stack
            as='main'
            horizontalAlign='center'
            disableShrink
            grow
        >
            {children}
        </Stack>
        <Footer />
    </Stack>
)

export default Layout
