import React from "react"
import { useSelector } from 'react-redux'
import { RootState } from '@/type/state'

const g = [
/* c */  "M309 9H49C26.9086 9 9 26.9086 9 49v420c0 22.091 17.9086 40 40 40h260",
/* i */  "M409 509V109",
/* t */  "M670.194 509V109m0 0H509m161.194 0H809",
/* a */  "M1159 509V149c0-22.091-17.91-40-40-40H949c-22.091 0-40 17.909-40 40v260m0 100V409m0 0l24.486-72.758C938.964 319.964 954.222 309 971.397 309h141.933",
/* g */  "M1609 9h-310c-22.09 0-40 17.9086-40 40v420c0 22.091 17.91 40 40 40h270c22.09 0 40-17.909 40-40V362.386c0-22.092-17.91-40-40-40h-163.88",
/* e */  "M1959 509h-210c-22.09 0-40-17.909-40-40V309m250-200h-210c-22.09 0-40 17.909-40 40v160m0 0h191.35",
/* r */  "M2059 509V109h260a40 40 0 0140 40v167.8a40 40 0 01-40 40h-181.12c-7.05 0-9.68 9.24-3.69 12.95L2359 509",
/* a */  "M2709 509V149a40 40 0 00-40-40h-170a40 40 0 00-40 40v260m0 100V409m0 0l24.49-72.76A39.99 39.99 0 012521.4 309h141.93",
/* d */  "M3009 500.5h-191.5v-383H3009c50.53 0 91.5 40.966 91.5 91.5v200c0 50.534-40.97 91.5-91.5 91.5z",
/* r */  "M3709 509V109h260c22.09 0 40 17.909 40 40v167.799c0 22.091-17.91 40-40 40h-181.12c-7.05 0-9.68 9.239-3.69 12.951L4009 509"
]

const linearGradient = [
    { x: 159, y: 9 },
    { x: 559, y: 109 },
    { x: 659, y: 109 },
    { x: 1034, y: 109 },
    { x: 1434, y: 9 },
    { x: 1834, y: 109 },
    { x: 2209, y: 109 },
    { x: 2584, y: 109 },
    { x: 2959, y: 109 },
    { x: 3859, y: 109 }
]

const LogoTipo = () => {
    const { loading } = useSelector((state: RootState) => state.searchList)
    return <g id={`${loading}-group`}/>
}

const pathLogo = {
    aspa: "M3419.13 312.82l12.08-68.95 63.41 11.256-11.11 63.434-44.94 78.047-33.09-5.873 39.15-73.387-25.5-4.527zm-80.64-14.314l12.08-68.951 63.41 11.256-11.11 63.435-44.94 78.046-33.09-5.872 39.15-73.387-25.5-4.527z",
    arco: "M3566.23 191.931c.33.73.43 1.484.34 2.194 26.58 32.964 42.43 74.481 42.43 119.592 0 107.23-89.54 194.157-200 194.157-45.35 0-87.17-14.649-120.71-39.339l-25.87 26.043c-2.17 2.185-5.88 1.185-6.67-1.799l-23.11-86.828c-.08-.279-.12-.556-.13-.828-15.01-27.24-23.51-58.359-23.51-91.406 0-107.23 89.54-194.157 200-194.157 32.81 0 63.77 7.67 91.09 21.263l22.04-31.257c1.77-2.519 5.59-2.152 6.88.659l37.22 81.706zm-314.09 213.048c-16.61-26.703-26.14-57.954-26.14-91.262 0-97.369 81.45-177.157 183-177.157 29.21 0 56.76 6.602 81.18 18.325l-19.73 27.989c-1.77 2.519-.16 6.031 2.89 6.32l72.44 6.842c28.79 31.36 46.22 72.639 46.22 117.681 0 97.37-81.45 177.157-183 177.157-40.76 0-78.28-12.853-108.6-34.539l25.15-25.319c2.17-2.185 1.18-5.915-1.78-6.715l-71.63-19.322z"
}

const filter = [
    { x: 0.5, y: 0.5, width: 331, height: 541 },
    { x: 400.5, y: 100.5, width: 31, height: 441 },
    { x: 500.5, y: 100.5, width: 331, height: 441 },
    { x: 900.5, y: 100.5, width: 281, height: 441 },
    { x: 1250.5, y: 0.5, width: 381, height: 541 },
    { x: 1700.5, y: 100.5, width: 281, height: 441 },
    { x: 2050.5, y: 100.5, width: 331, height: 441 },
    { x: 2450.5, y: 100.5, width: 281, height: 441 },
    { x: 2809, y: 109, width: 314, height: 424 },
    { x: 3700.5, y: 100.5, width: 331, height: 441 }
]


//    { x: 3205, y: 107.878, width: 408, height: 407.996, dx:0, dy:4 },
//    { x: 3320.84, y: 229.555, width: 177.771, height: 175.052, dx:0, dy:4 },
const Logo = () => (
    <svg viewBox="0 0 4032 542" fill="none" xmlns="http://www.w3.org/2000/svg">
        {g.map((path: string, n: number) => (
            <g key={path} filter={`url(#filter${n})`} strokeWidth={17} strokeLinecap="round" strokeLinejoin="bevel">
                <path d={path} stroke="#0A4051" />
                <path d={path} stroke={`url(#linear${n})`} />
            </g>
        ))}
        <LogoTipo/>
        <g filter="url(#filter_aspa)">
            <path d={pathLogo.aspa} fill="#0A4051" />
            <path d={pathLogo.aspa} fill="url(#linear_aspa)" />
        </g>
        <g filter="url(#filter_arco)" fillRule="evenodd" clipRule="evenodd">
            <path d={pathLogo.arco} fill="#0A4051" />
            <path d={pathLogo.arco} fill="url(#linear_arco)" />
        </g>
        <defs>
            {linearGradient.map(({ x, y }, n: number) => (
                <linearGradient key={x} id={`linear${n}`} x1={x} y1={y} x2={x} y2={509} gradientUnits="userSpaceOnUse">
                    <stop stopColor="#fff" />
                    <stop offset={1} stopColor="#fff" stopOpacity={0} />
                </linearGradient>
            ))}
            <linearGradient id="linear_aspa" x1={3409.73} y1={229.56} x2={3409.73} y2={396.61} gradientUnits="userSpaceOnUse">
                <stop stopColor="#fff" />
                <stop offset={1} stopColor="#fff" stopOpacity={0} />
            </linearGradient>
            <linearGradient id="linear_arco" x1={3409} y1={107.88} x2={3409} y2={507.87} gradientUnits="userSpaceOnUse">
                <stop stopColor="#fff" />
                <stop offset={1} stopColor="#fff" stopOpacity={0} />
            </linearGradient>
            {filter.map(({ x, y, width, height }, n: number) => (
                <filter
                    key={x}
                    id={`filter${n}`}
                    x={x}
                    y={y}
                    width={width}
                    height={height}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    />
                    <feOffset dx={7} dy={17} />
                    <feGaussianBlur stdDeviation={3.5} />
                    <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                    <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
                    <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
                </filter>
            ))}

            <filter
                id="filter_aspa"
                x={3320.84} y={229.555} width={177.771} height={175.052}
                filterUnits="userSpaceOnUse"
                colorInterpolationFilters="sRGB"
            >
                <feFlood floodOpacity={0} result="BackgroundImageFix" />
                <feColorMatrix
                    in="SourceAlpha"
                    values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                />
                <feOffset dy={4} />
                <feGaussianBlur stdDeviation={2} />
                <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
                <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
            </filter>
            <filter
                id="filter_arco"
                x={3205} y={107.878} width={408} height={407.996}
                filterUnits="userSpaceOnUse"
                colorInterpolationFilters="sRGB"
            >
                <feFlood floodOpacity={0} result="BackgroundImageFix" />
                <feColorMatrix
                    in="SourceAlpha"
                    values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                />
                <feOffset dy={4} />
                <feGaussianBlur stdDeviation={2} />
                <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
                <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
            </filter>
        </defs>
    </svg>
)

export default Logo