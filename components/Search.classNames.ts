import { mergeStyleSets, memoizeFunction } from '@fluentui/react'
import { buttonStyles } from './styles/ButtonStyles'
import { DefaultEffects, FontSizes } from '@fluentui/react'

const container = {
    ':focus': {
        outline: '#4ef0f7 1px ridge'
    }
}

const box = {
    width: '100%',
    height: '100%',
    color: '#0A4051',
    background: 'radial-gradient( circle farthest-corner at 50%,  #fafdff 25%, #f3f3f3 100% )',
    fontSize: FontSizes.xLarge,
    boxShadow: DefaultEffects.elevation16,
    borderRadius: DefaultEffects.roundedCorner6
}

export const getSearchClassNames = memoizeFunction((show: boolean) => mergeStyleSets({
    box: [
        box,
        show && {
            borderRadius: '6px 6px 0px 0px'
        }
    ],
    container,
    buttonStyles
}))
