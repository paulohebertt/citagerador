import * as React from 'react'
import SearchListBox from './SearchListBox'
import { Stack, SearchBox, DefaultButton, PrimaryButton, ISearchBoxProps } from '@fluentui/react'
import { useDispatch } from 'react-redux'
import { getRandom, getSearch } from 'utils/fetchers'
import * as icones from 'utils/icones.json'
import { reset } from 'lib/actions'
import { useRouter } from 'next/router'
import { AppDispatch } from '@/type/state'
import { getSearchClassNames } from './Search.classNames'
import { gap10 } from '@/styles/tokens'

const Search = ({ isHomePage = false }: { isHomePage: boolean }) => {
    const [text, setText] = React.useState('')
    const [show, setShow] = React.useState(false)
    const classes = getSearchClassNames(show)
    const dispatch = useDispatch<AppDispatch>()
    const router = useRouter()
    const textDispatch = (newValue: string) => {
        setText(newValue)
        const { length } = newValue
        if (length > 2) {
            dispatch(getSearch(newValue))
        } else if (length === 0) {
            dispatch(reset())
        }
    }
    const textChange: ISearchBoxProps['onChange'] = (...args) => textDispatch(args[1] || '')
    const textClear = () => textDispatch('')
    const textRandom = () => getRandom().then(textDispatch)
    const routerSearch = () => {
        router.push({
            pathname: '/search',
            query: { text: encodeURIComponent(text) }
        })
    }
    let closeTimeOut: number
    const close = () => {
        closeTimeOut = setTimeout(() => setShow(false))
    }
    const notClose = () => {
        clearTimeout(closeTimeOut)
    }
    const open = () => {
        setShow(true)
        notClose()
    }
    return (
        <Stack horizontal={!isHomePage} tokens={gap10}>
            <Stack
                disableShrink
                tabIndex={-1}
                onFocus={notClose}
                onBlur={close}
                className={classes.container}
            >
                <SearchBox
                    underlined={true}
                    className={classes.box}
                    ariaLabel={'Caixa de Pesquisar'}
                    onFocus={open}
                    onSearch={routerSearch}
                    onChange={textChange}
                    onClear={textClear}
                    value={text}
                />
                {show && <SearchListBox />}
            </Stack>
            {isHomePage ? (
                <Stack horizontal horizontalAlign="space-evenly">
                    <PrimaryButton
                        text="Pesquisar"
                        className={classes.buttonStyles}
                        //styles={buttonLeftAppStyles}
                        menuIconProps={icones.Send}
                        onClick={routerSearch}
                    />
                    <DefaultButton
                        text="Aleatório"
                        className={classes.buttonStyles}
                        //styles={buttonLeftAppStyles}
                        iconProps={icones.SyncOccurence}
                        onClick={textRandom}
                    />
                </Stack>
            ) : (
                    <PrimaryButton
                        className={classes.buttonStyles}
                        menuIconProps={icones.Send}
                        onClick={routerSearch}
                    />
                )}
        </Stack>
    )
}

export default Search
