import { mergeStyleSets } from '@fluentui/react'
import { DefaultEffects, MotionAnimations } from '@fluentui/react'

const renderStack = {
    cursor: 'pointer',
    padding: 10,
    ':hover': {
        borderLeft: '4px solid #005a9e',
        background: '#ededed'
    },
    '& strong': {
        fontWeight: 'bold'
    }
}

const icon = {
    color: '#778899',
    fontSize: 40,
    height: 40,
    width: 40
}

const container = {
    width: '100%',
    minHeight: 50,
    maxHeight: 250,
    overflowY: 'auto',
    background: 'radial-gradient( circle farthest-corner at 50%,  #fafdff 25%, #f3f3f3 100% )',
    boxShadow: DefaultEffects.elevation16,
    borderRadius: `0px 0px ${DefaultEffects.roundedCorner6} ${DefaultEffects.roundedCorner6}`,
    animation: MotionAnimations.fadeIn,
    zIndex: 2,
    scrollbarWidth: 'thin',
    scrollbarcolor: '#49CAF5 #EDEDED',
    '::-webkit-scrollbar': {
        width: 14
    },
    '::-webkit-scrollbar-thumb': {
        background: 'radial-gradient(circle, #49caf5 0%,#41a7c9 100%)',
        borderRadius: 7
    },
    '::-webkit-scrollbar-thumb:hover': {
        background: 'radial-gradient(circle, #4a97b0 0%,#005875 100%)'
    },
    '::-webkit-scrollbar-thumb:active': {
        background: 'radial-gradient(circle, #c0e4f0 0%,#49caf5 100%)'
    },
    '::-webkit-scrollbar-track': {
        background: '#ededed',
        borderRadius: 7
    }
}

const child = {
    '& > p, .ms-Spinner': {
        fontSize: 17,
        textAlign: 'center',
        padding: 10
    }
}

const item = {
    paddingRight: 7,
    paddingLeft: 7
}

export const getSLBClassNames = () => mergeStyleSets({
    container,
    renderStack,
    child,
    item,
    icon
})
