import { Stack, List, Text, Spinner, SpinnerSize, Icon, IListProps } from '@fluentui/react'
import { ListItem } from '@/type/state'
import Link from 'next/link'
import ImageWiki from './ImageWiki'
import { useQueryList } from 'lib/hooks'
import { getSLBClassNames } from './SearchListBox.classNames'

const SearchListBox = () => {
  const { totalhits, list, loading, error } = useQueryList()
  const classes = getSLBClassNames()
  const renderList = ({ pageid, title, snippet, matchedTitle, matchedPage }: ListItem) => (
    <Link
      key={title}
      href={{
        pathname: '/search',
        query: { id: pageid, text: encodeURIComponent(title) }
      }}
      passHref
      data-is-focusable={true}
    >
      <Stack
        as='a'
        className={classes.renderStack}
        horizontal
      >
        {matchedPage && <ImageWiki id={pageid} title={title} />}
        <Stack.Item className={classes.item} grow>
          <Text variant='mediumPlus' block>
            {matchedTitle.length > 0 ? matchedTitle.map(({ text, matched }) => matched ? (
              <strong key={text}>{text}</strong>
            ) : text) : title}
          </Text>
          {matchedPage && <Text as='p' variant='medium'>{snippet}...</Text>}
        </Stack.Item>
        {matchedPage && <Icon iconName={'RibbonSolid'} className={classes.icon} />}
      </Stack>
    </Link>
  )
  return (
    <Stack className={classes.container}>
      <Stack.Item className={classes.child}>
        {loading ?
          <Spinner
            size={SpinnerSize.large}
            label="Carregando..."
            ariaLive="assertive"
          /> : error ?
            <p>
              <Icon iconName="Warning" className={classes.icon} />
              {'Ops..., Tente Novamente'}
            </p> : totalhits !== 0 ?
              <List
                items={list}
                onRenderCell={renderList as IListProps<ListItem>['onRenderCell']}
              /> : list.length !== 0 ?
                <p>{'Digite um nome'}</p> :
                <p>{'Nenhum resultado encontrado'}</p>
        }
      </Stack.Item>
    </Stack >
  )
}

export default SearchListBox
