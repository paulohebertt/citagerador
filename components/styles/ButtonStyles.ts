import { IButtonStyles, DefaultEffects } from '@fluentui/react'

const buttonLeftAppStyles: IButtonStyles = {
    root: {
        background: 'radial-gradient( circle farthest-corner at 40% 10%, #239ce2 0%, #007caa 100%)',
        boxShadow: DefaultEffects.elevation16,
        color: '#f7f7f7',
        borderRadius: 17,
    },
    rootHovered: {
        color: '#f7f7f7',
        background: 'radial-gradient( circle farthest-corner at 40% 100%, #3aa4e2 0%, #1195c5 100%)'
    },
    rootPressed: {
        color: '#f7f7f7',
        background: 'radial-gradient(circle at 40% 10%, #08679e 0%, #004b67 100%)'
    }
}

const buttonRightAppStyles: IButtonStyles = {
    root: {
        boxShadow: DefaultEffects.elevation4,
        borderRadius: 17,
    }
}

const buttonStyles = {
    boxShadow: DefaultEffects.elevation16,
    borderRadius: 17
}

export { buttonLeftAppStyles, buttonRightAppStyles, buttonStyles }
