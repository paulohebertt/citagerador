import { ITextStyles } from '@fluentui/react'

const textAppStyles: ITextStyles = {
    root: {
        textAlign: 'center',
        color: '#007caa',
        paddingBottom: 4,
        textShadow: '4px 7px 4px rgba(100, 179, 197, 0.4)'
    }
}

const textCenter = {
    textAlign: 'center'
}

export { textAppStyles, textCenter }
