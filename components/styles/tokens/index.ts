import { IStackTokens } from '@fluentui/react'

export const homeStackTokens: IStackTokens = {
    childrenGap: 30,
    padding: 10,
    maxWidth: 640
}

export const gap10: IStackTokens = {
    childrenGap: 10
}
