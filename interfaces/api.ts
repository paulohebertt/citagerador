import { ListState } from './state'

export type RandomTypes = {
    batchcomplete: string,
    continue?: {
        rncontinue: string,
        continue: string
    },
    query: {
        random: [{
            id: number,
            ns: number,
            title: string
        }]
    }
}

export type SearchTypes = {
    batchcomplete: string,
    continue?: {
        sroffset: string,
        continue: string
    },
    query: {
        searchinfo: {
            totalhits: number,
            suggestion?: string
            suggestionsnippet?: string,
            rewrittenquery?: string,
            rewrittenquerysnippet?: string
        },
        search: ListState[]
    }
}

export type ImageTypes = {
    batchcomplete: string,
    query: {
        pages: {
            [name: string]: {
                pageid: number,
                ns: 0,
                title: string,
                thumbnail?: {
                    source: string,
                    width: number,
                    height: number
                }
            }
        }
    }
}