import rootReducer from 'lib/reducers'
import store from 'lib/store'

export type RootState = ReturnType<typeof rootReducer>
export type AppDispatch = typeof store.dispatch

export type ListState = {
    ns: 0,
    pageid: number,
    title: string,
    snippet: string,
    titlesnippet: string
}

export type SearchListState = {
    totalhits: number,
    suggestion: string
    rewrittenquery: string,
    list: ListState[],
    loading: boolean,
    error: boolean
}

export type ObjTitle = {
    text: string,
    matched: boolean
}

export type ListItem = {
    pageid: number,
    title: string,
    snippet: string,
    matchedTitle: ObjTitle[],
    matchedPage: boolean
}

export type QueryListState = Exclude<SearchListState, 'list'> & {
    list: ListItem[]
}
