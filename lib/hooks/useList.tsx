import React from "react"
import { useSelector } from 'react-redux'
import { searchList } from 'lib/selectors'
import { searchMatch, initialMatch, quotEntity } from 'utils/regex'

const useList = () => {
  const { totalhits, list, loading, error } = useSelector(searchList)
  const getArray = (str: string) => {
    const arr: (JSX.Element | string)[] = []
    const initialTest = initialMatch.test(str)
    const test = searchMatch.test(str)
    if (test) {
      let match: RegExpExecArray | null
      let count = 0
      while ((match = searchMatch.exec(str)) !== null) {
        if (match.index === searchMatch.lastIndex) {
          searchMatch.lastIndex++;
        }
        match.forEach((_m) => {
          const bool = initialTest && count % 2 !== 0
          if (bool) {
            return arr.push(<strong>{_m}</strong>)
          } else {
            return arr.push(_m)
          }
        })
        count++
      }
    }
    return arr
  }
  const newList = list.map(({ title, pageid, snippet, titlesnippet }) => {
    const newSnippet = getArray(snippet.replace(quotEntity, "\""))
    const newTitleSnippet = getArray(titlesnippet)
    return {
      title,
      pageid,
      snippet: newSnippet,
      titlesnippet: newTitleSnippet
    }
  })
  return {
    list: newList,
    totalhits,
    loading,
    error
  }
}

export default useList
