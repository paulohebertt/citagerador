import { useSelector } from 'react-redux'
import { searchList } from 'lib/selectors'
import { ObjTitle } from '@/type/state'
import { searchMatch, initialMatch, quotEntity, fullMatch, searchMatchClear } from 'utils/regex'

const useQueryList = () => {
  const data = useSelector(searchList)
  const getNewTitle = (str: string) => {
    const arr: ObjTitle[] = []
    const initialTest = initialMatch.test(str)
    let match: RegExpExecArray | null
    let count = 0
    while ((match = searchMatch.exec(str)) !== null) {
      count++
      if (match.index === searchMatch.lastIndex) {
        searchMatch.lastIndex++
      }
      match.forEach((text) => {
        const matched = (initialTest && count % 2 !== 0) || (!initialTest && count % 2 === 0)
        arr.push({ text, matched })
      })
    }
    return arr
  }
  const getNewSnippet = (str: string) => {
    return str
      .replace(quotEntity, '\"')
      .replace(searchMatchClear, '')
  }
  const newList = data.list.map(({ pageid, title, titlesnippet, snippet }) => {
    const matchedTitle = getNewTitle(titlesnippet)
    const newSnippet = getNewSnippet(snippet)
    const matchedPage = fullMatch.test(titlesnippet)
    return {
      pageid,
      title,
      matchedTitle,
      matchedPage,
      snippet: newSnippet
    }
  })
  return {
    ...data,
    list: newList,
  }
}

export default useQueryList
