import { combineReducers } from '@reduxjs/toolkit'
import searchList from './slices/SearchList'

const rootReducer = combineReducers({
    searchList
})

export default rootReducer

export type RootState = ReturnType<typeof rootReducer>
