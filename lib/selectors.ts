import { RootState } from '@/type/state'

export const searchList = (state: RootState) => state.searchList
