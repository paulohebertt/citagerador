import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { SearchListState, ListState } from '@/type/state'

const list: ListState[] = [{
  ns: 0,
  pageid: 0,
  title: '',
  snippet: '',
  titlesnippet: ''
}]

const initialState: SearchListState = {
  totalhits: 0,
  suggestion: '',
  rewrittenquery: '',
  list,
  loading: false,
  error: false,
}

const searchListSlice = createSlice({
  name: 'searchList',
  initialState,
  reducers: {
    setList: (state: SearchListState, action: PayloadAction<Pick<SearchListState, 'list' | 'totalhits' | 'suggestion' | 'rewrittenquery'>>) => {
      state.loading = false
      state.error = false
      state.list = action.payload.list
      state.totalhits = action.payload.totalhits
      state.suggestion = action.payload.suggestion
      state.rewrittenquery = action.payload.rewrittenquery
    },
    reset: (state: SearchListState) => {
      state.list = list
      state.totalhits = 0
      state.suggestion = ''
      state.rewrittenquery = ''
    },
    start: (state: SearchListState) => {
      state.loading = true
      state.error = false
    },
    failed: (state: SearchListState) => {
      state.error = true
      state.loading = false
    }
  }
})

export const { setList, reset, start, failed } = searchListSlice.actions
export const { name } = searchListSlice
export default searchListSlice.reducer
