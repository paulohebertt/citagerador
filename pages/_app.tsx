import { StrictMode } from 'react'
import { AppProps } from 'next/app'
import Head from 'next/head'
import Layout from '@/Layout'
import ErrorBoundary from '@/ErrorBoundary'
import { Provider } from 'react-redux'
import { theme } from 'utils/theme'
import store from "lib/store"
import { initializeIcons, ThemeProvider } from '@fluentui/react'
import 'reset.css'

initializeIcons()

function App({ Component, pageProps }: AppProps) {
    return (
        <StrictMode>
            <Head>
                <meta name="viewport" content="initial-scale=1.0, width=device-width, viewport-fit=cover" />
            </Head>
            <ErrorBoundary>
                <Provider store={store}>
                    <ThemeProvider theme={theme} applyTo='body'>
                        <Layout>
                            <Component {...pageProps} />
                        </Layout>
                    </ThemeProvider>
                </Provider>
            </ErrorBoundary>
        </StrictMode>
    )
}

export default App
