import Document, { DocumentContext, Html, Head, Main, NextScript } from 'next/document'
import { Stylesheet, InjectionMode, resetIds } from '@fluentui/react'

const styleSheet = Stylesheet.getInstance()

styleSheet.setConfig({
  injectionMode: InjectionMode.none,
  namespace: "cg"
})

class AppDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    styleSheet.reset()
    resetIds()
    const initialProps = await Document.getInitialProps(ctx)
    return {
      ...initialProps,
      styles: (
        <style type="text/css" data-merge-styles dangerouslySetInnerHTML={{ __html: styleSheet.getRules(true) }}/>
      )
    }
  }
  render() {
    return (
      <Html lang="pt-BR">
        <Head>
          <meta charSet="utf-8" />
          <link rel="icon" href="/citagerador/favicon.png" type="image/png" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default AppDocument
