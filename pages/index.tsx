import React from 'react'
import { Stack } from '@fluentui/react'
import Search from '@/Search'
import Logo from '@/Logo'
import Head from 'next/head'
import { homeStackTokens } from '@/styles/tokens'


const App = () => (
    <>
        <Head>
            <title>CitaGerador👋</title>
        </Head>
        <Stack
            tokens={homeStackTokens}
            verticalAlign='center'
            disableShrink
            verticalFill
            grow
        >
            <Logo />
            <Search isHomePage />
            {/*<Stack horizontal horizontalAlign="space-evenly">
                <DefaultButton
                    text="Pesquisar"
                    styles={buttonLeftAppStyles}
                    menuIconProps={icones.Send}
                    onClick={routerSearch}
                />
                <DefaultButton
                    text="Aleatório"
                    styles={buttonRightAppStyles}
                    iconProps={icones.SyncOccurence}
                    onClick={random}
                />
            </Stack>*/}
        </Stack>
    </>
)

export default App
