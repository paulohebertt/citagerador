import { useRouter } from 'next/router'

enum PropsQuery {
    Text = 'text',
    Id = 'id'
}

const RedirectHome = () => {
    return <p/>
}

const SearchPage = () => {
    const { query } = useRouter()
    const hasId = query.hasOwnProperty(PropsQuery.Id)
    const hasTitle = query.hasOwnProperty(PropsQuery.Text)
    if (hasId || hasTitle) {
        return
    }
    return <RedirectHome />
}

export default SearchPage
