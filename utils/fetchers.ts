import { RandomTypes, SearchTypes, ImageTypes } from '@/type/api'
import { AppDispatch } from '@/type/state'
import { setList, start, failed } from 'lib/actions'

const urlRandom = "https://pt.wikiquote.org/w/api.php?action=query&format=json&list=random&utf8=1&rnnamespace=0&origin=*"

const getRandom = async () => {
    try {
        const res = await fetch(urlRandom)
        if (res.ok) {
            const bd: RandomTypes = await res.json()
            const { random } = bd.query
            return random[0].title
        } else {
            return 'Erro: Algo inexplicável ocorreu'
        }
    } catch (err) {
        return 'Erro:' + err.message
    }
}


const urlSearch = "https://pt.wikiquote.org/w/api.php?action=query&format=json&list=search&utf8=1&srprop=snippet%7Ctitlesnippet&srenablerewrites=1&origin=*&"

const getSearch = (title: string) => async (dispatch: AppDispatch) => {
    dispatch(start())
    try {
        const res = await fetch(urlSearch + `srsearch=${title}`)
        if (res.ok) {
            const bd: SearchTypes = await res.json()
            const { search, searchinfo } = bd.query
            dispatch(setList({
                list: search,
                totalhits: searchinfo.totalhits,
                suggestion: searchinfo.suggestion || '',
                rewrittenquery: searchinfo.rewrittenquery || '',
            }))
        }
    } catch (err) {
        dispatch(failed())
    }
}

const urlImage = 'https://pt.wikiquote.org/w/api.php?action=query&format=json&prop=pageimages&utf8=1&piprop=thumbnail&origin=*&'

const getImage = async (id: number, firstText: string) => {
    try {
        const res = await fetch(urlImage + `pageids=${id}`)
        if (res.ok) {
            const bd: ImageTypes = await res.json()
            const { pages } = bd.query
            if ('thumbnail' in pages[id]) {
                return pages[id].thumbnail
            }
        }
    } catch (err) {
        console.log('Erro:' + err.message)
    }
    return {
        source: 'https://via.placeholder.com/50/34568B/ffffff/?text=' + firstText,
        width: 50,
        height: 50
    }
}

export { getRandom, getSearch, getImage }
