export const searchMatch = /[^<>]+(?=<)|[^>]+$/g

export const initialMatch = /^(<span[^"]+\"searchmatch\")/

export const fullMatch = /^(<span).+(<\/span>\)?)$/

export const searchMatchClear = /<(span|\/)([^>]+|(\"searchmatch\")$)>/g

export const quotEntity = /\&quot\;/g
