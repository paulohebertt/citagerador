import { PartialTheme } from '@fluentui/react'

export const theme: PartialTheme = {
    palette: {
        themePrimary: '#0A4051',
    }
    /*components
    defaultFontStyle
    disableGlobalClassNames
    effects
    fonts
    isInverted
    rtl
    semanticColors
    spacin*/
}

